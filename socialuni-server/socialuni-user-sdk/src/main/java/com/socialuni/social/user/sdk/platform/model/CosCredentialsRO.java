package com.socialuni.social.user.sdk.platform.model;

import lombok.Data;

@Data
public class CosCredentialsRO {
    String tmpSecretId;
    String tmpSecretKey;
    String sessionToken;
}
